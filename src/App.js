import React from 'react';
import {BrowserRouter, Route, Switch} from "react-router-dom";
import Navbar from "./components/Navbar/Navbar";
import SubmitQuote from "./components/Submit-quote/Submit-quote";
import Quotes from "./containers/Quotes/Quotes";



const App = () => {
    return (
        <BrowserRouter>
            <Navbar/>
            <Switch>
                <Route exact path="/" component={Quotes}/>
                <Route path="/quotes/:id" component={Quotes}/>
                <Route path="/quote/edit/:action" component={Quotes}/>
                <Route path="/new-quote" component={SubmitQuote}/>
            </Switch>

        </BrowserRouter>


        );
};

export default App;