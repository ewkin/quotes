import axios from "axios";

const axiosQuote = axios.create({
    baseURL: 'https://js9-quotes-default-rtdb.firebaseio.com/'
});

export default axiosQuote;