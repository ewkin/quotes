
export const CATEGORIES = [
    {title: 'Star Wars', id: 'star-wars'},
    {title: 'Famous People', id: 'famous-people'},
    {title: 'Sayings', id: 'sayings'},
    {title: 'Humor', id: 'humor'},
    {title: 'Motivational', id: 'motivational'},
]

export const toArr = object => {
    return Object.keys(object).map((key)=>{
        return {...object[key], 'id': key} ;
    })
};
