import React from 'react';
import {NavLink} from "react-router-dom";
import './Navbar.css';

const Navbar = () => {
    return (
        <div>
            <nav className="Nav">
                <div>Quotes Central</div>
                <div><NavLink to="/">Home</NavLink></div>
                <div>
                    <NavLink
                        to="/new-quote"
                        activeClassName="selectedLink">
                        Submit new quote
                    </NavLink>
                </div>
            </nav>
        </div>
    );
};

export default Navbar;