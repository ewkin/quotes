import React, {useEffect, useState} from 'react';
import axiosQuote from '../../axios-quote';
import {CATEGORIES} from '../../constants';

const SubmitQuote = props => {
    const [quote, setQuote] = useState({
        author: '',
        quote: '',
        category:'star-wars',
    });

    useEffect(()=>{
        if(props.match.params.action){
            const fetchData = async () =>{
                const response = await axiosQuote.get('/quotes/'+props.match.params.action+'.json');
                setQuote(response.data);
            };
            fetchData().catch(console.error);
        }
    },[props.match.params.action])

    const quoteInput = event => {
        const {name, value} = event.target;
        setQuote(prevState => ({
            ...prevState,
            [name]: value,
        }));
    };

    const postQuoteHandler = async event =>{
        event.preventDefault();
        if(props.match.params.action){
            try {
                await axiosQuote.put('/quotes/'+props.match.params.action+'.json', {...quote});
            } finally {
                props.history.goBack('/');
            }

        } else {
            try {
                await axiosQuote.post('/quotes.json', {...quote});
            } finally {
                props.history.replace('/');
            }
        }
    };

    let formName = (<h2>Submit new Quote</h2>);
    if(props.match.params.action){
        formName = (<h2>Edit quote</h2>)
    }
    return (
        <>
            {formName}
            <form onSubmit={postQuoteHandler}>
                <div>
                    <label>
                        Category:
                        <select name="category" id="category" onChange={quoteInput}>
                        {CATEGORIES.map((category) => (
                            <option key={category.id} value={category.id}>{category.title}</option>
                        ))}
                    </select>
                    </label>
                </div>
                <div>
                    <label>
                        Author:
                        <input
                            required placeholder="Author"
                            type="text" name="author"
                            value={quote.author} onChange={quoteInput}
                        />
                    </label>
                </div>
                <div>
                    <label>
                        Quote:
                        <textarea
                            required placeholder="New quote"
                            type="text" name="quote"
                            value={quote.quote} onChange={quoteInput}
                        />
                    </label>
                </div>
                <button type="submit" className="Button">Save</button>
            </form>
        </>
    );
};

export default SubmitQuote;