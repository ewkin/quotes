import React from 'react';
import {CATEGORIES} from "../../constants";
import {BrowserRouter as Router, Switch, Route, Link} from "react-router-dom";
import Quote from "../Quote/Quote";
import SubmitQuote from "../../components/Submit-quote/Submit-quote";


const Quotes = () => {
    return (
        <Router>
            <div style={{ display: "flex" }}>
                <div
                    style={{
                        padding: "10px",
                        width: "40%",
                        background: "#f0f0f0"
                    }}
                >
                    <ul style={{listStyleType: "none", padding: 0 }}>
                        <li>
                            <Link to={'/'}>All</Link>
                        </li>
                        {CATEGORIES.map((category) => (
                            <li>
                                <Link key={category.id} to={'/quotes/'+category.id}>{category.title}</Link>
                            </li>
                        ))}
                    </ul>
                </div>
                <div style={{ flex: 1, padding: "10px" }}>
                    <Switch>
                        <Route
                            path={'/'}
                            exact
                            render={props =>(
                                <Quote  {...props}/>
                            )}
                        />
                        <Route
                            path={'/quotes/:id'}
                            render={props =>(
                                <Quote {...props}/>
                            )}
                        />
                        <Route
                            path={'/quote/edit/:action'}
                            render={props =>(
                                <SubmitQuote  {...props}/>
                            )}
                        />
                    </Switch>
                </div>
            </div>
        </Router>
    );
};

export default Quotes;