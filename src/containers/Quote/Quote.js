import React, {useEffect, useState} from 'react';
import axiosQuote from "../../axios-quote";
import {CATEGORIES, toArr} from "../../constants";
import {useLocation} from "react-router-dom";

import "./Quote.css";

const Quote = props => {
    const [category, setCategory] = useState([]);
    const [header, setHeader] = useState('All');

    const location = useLocation()

    useEffect(()=>{
        if(props.match.params.id){
            let id = CATEGORIES.filter(category => {
                return category.id === props.match.params.id
            });
            setHeader(id[0].title);
            const fetchData = async () =>{
                const response = await axiosQuote.get('/quotes.json?orderBy="category"&equalTo="'+props.match.params.id+'"');
                setCategory(toArr(response.data));
            }
            fetchData().catch(console.error);
        } else {
            const fetchData = async () =>{
                const response = await axiosQuote.get('/quotes.json');
                setCategory(toArr(response.data));
            }
            fetchData().catch(console.error);
            setHeader('All');
        }
    },[props.match.params.id, location]);

    const editPost = id => {
        props.history.push({
            pathname: '/quote/edit/'+id,
        });
    };

    const deleteQuote = id=>{
        const fetchData = async () =>{
            // eslint-disable-next-line no-unused-vars
            const response = await axiosQuote.delete('/quotes/'+id+'.json');
        };
        fetchData().catch(console.error);
        let idToDelete = category.findIndex((quote) =>{
            return quote.id === id
        });
        let quotesCopy = [...category];
        quotesCopy.splice(idToDelete,1);
        setCategory(quotesCopy);
    };


    return (
        <div>
            <h3 className="Quote-header">{header}</h3>
            {category.map((quote) => (
                <article className="Quote" key={quote.id}>
                    <div className="Quote-body">
                        <span>"{quote.quote}"</span>
                        <div>
                            <button onClick={()=>editPost(quote.id)}>Edit</button>
                            <button onClick={()=> deleteQuote(quote.id)}>x</button>
                        </div>
                    </div>
                    <div>-{quote.author}</div>
                </article>
            ))}
        </div>
    );
};

export default Quote;